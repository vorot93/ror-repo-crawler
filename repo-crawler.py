#!/usr/bin/python

import datetime
import re

import requests
from bs4 import BeautifulSoup

import os
import pytoml

import argparse

import helpers

URL_PREFIX = 'http://www.rigsofrods.com/repository/index/limit:50/'
DL_PREFIX = 'http://www.rigsofrods.com/repository/download/'

def get_pages_num(html_text):
    last_page_link = BeautifulSoup(html_text, 'html.parser').find("a", text="Last >>")
    total_pages = int(last_page_link["href"].split("page:")[1].split('/')[0])
    return total_pages


def get_entry_html_tables(html_text):
    entry_html_tables = []
    # body_wrapper = bs_object.body.find("div", class_="body_wrapper")
    # entry_list_html = body_wrapper.table.tbody.tr.td
    # entry_list_html = re.sub('<!-- PAGINATOR START -->*?<!-- PAGINATOR END -->','',str(entry_list_html), flags=re.DOTALL)
    # print(entry_list_html)
    # return metadata, description, tags
    entry_table_list = BeautifulSoup(html_text, 'html.parser').find_all("table", class_="filetable")
    total_entries = len(entry_table_list)
    for entry_num in range(total_entries):
        entry_html_tables.append({})

        entry_object = BeautifulSoup(str(entry_table_list[entry_num]), 'html.parser')

        # Extract tables from HTML
        desc_table = entry_object.find("tbody").tr.find_all("td", class_="filetablebg2", recursive=False)[0]
        try:
            tags_table_contents = []
            for tag in entry_object.find("tbody").find_next_sibling().tr.td.table.tr.find_all("td", recursive=False)[-2].stripped_strings:
                tags_table_contents.append(tag)
            tags_table_contents = tuple(set(tags_table_contents))
        except IndexError:
            print(entry_num, "'s tags set to None")
            tags_table_contents = None

        # Split description and meta
        meta_table_contents = desc_table.table.extract().tr.extract()
        desc_table_contents = desc_table.extract()

        # Add to DB
        entry_html_tables[entry_num]["meta_table"] = meta_table_contents.prettify()
        entry_html_tables[entry_num]["desc_table"] = desc_table_contents.prettify()
        entry_html_tables[entry_num]["tags_table"] = []
        if tags_table_contents is not None:
            for i in range(len(tags_table_contents)):
                if tags_table_contents[i] != ',':
                    entry_html_tables[entry_num]["tags_table"].append(tags_table_contents[i])
    return entry_html_tables


def parse_html_entry(html_content, content_set):
    for entry in html_content:
        meta_table_bs = BeautifulSoup(entry['meta_table'], 'html.parser')
        desc_table_bs = BeautifulSoup(entry['desc_table'], 'html.parser')

        dl_link = meta_table_bs.find("a", title="download file")
        details_link = meta_table_bs.find("a", title="view details")
        # date_unf = meta_table_bs.prettify().split('uploaded on')[1].split('by')[0].strip()

        entry_id = str(int(dl_link['href'].split('/')[-1]))

        file_name = dl_link.contents[-1].strip('\n ')
        # upload_date = datetime.datetime.strptime(date_unf, '%d/%m/%y')
        author = meta_table_bs.prettify().split('uploaded')[1].split('by')[1].split('</a>')[0].split('>')[-1].strip()
        ror_version = meta_table_bs.span.string.split('RoR')[-1].strip()

        desc_table_bs.find("a", {"name": "desc"}).decompose()

        # Save to content table
        content_set[entry_id] = {}
        content_set[entry_id]['file_name'] = file_name
        # content_set[entry_id]['upload_date'] = upload_date
        content_set[entry_id]['author'] = author
        content_set[entry_id]['ror_version'] = ror_version

        content_set[entry_id]['description'] = desc_table_bs.text

        content_set[entry_id]['tags'] = entry['tags_table']


def scan_page(html_text, content_set):
    html_content = get_entry_html_tables(html_text)
    parse_html_entry(html_content, content_set)


def get_content(repo_url):
    print("Started scraping repository:", URL_PREFIX)
    total_pages = get_pages_num(requests.get(URL_PREFIX).text)
    print("Total number of pages to scan:", total_pages)
    print("Starting scrape")
    # Download pages
    page = []
    for i in range(total_pages):
        page_num = i + 1
        print("Downloading page:", page_num)
        download = requests.get(URL_PREFIX + "page:" + str(page_num))
        if download.status_code == 200:
            page.append(download)
        else:
            print("Page", page_num, " download failed with response:", page.status_code)
            page.append(None)

    # Scan pages
    content_set = {}
    for i in range(total_pages):
        page_num = i + 1
        print("Scanning page:", page_num)
        if page[i] is None:
            print("Skipping page", page_num)
        else:
            scan_page(page[i].text, content_set)
    return content_set

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('folder', metavar='Folder', type=str, help='Folder where to dump data')
    args = parser.parse_args()
    metafile = os.path.join(os.path.expanduser(args.folder), 'file_metadata.toml')
    dl_list = os.path.join(os.path.expanduser(args.folder), 'download_list.txt')

    content_set = get_content(URL_PREFIX)

    helpers.save_table(metafile, content_set)
    with open(dl_list, "w") as text_file:
        for num in content_set:
            print(DL_PREFIX + num, file=text_file)
