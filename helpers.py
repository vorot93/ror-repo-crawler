import pytoml

def load_table(path):
        """Loads settings table into dict"""
        try:
            table_open_object = open(path, 'r')
        except FileNotFoundError:
            return None
        table = pytoml.load(table_open_object)
        return table


def save_table(path, data):
    """Saves settings to a file"""
    try:
        table_open_object = open(path, 'w')
    except FileNotFoundError:
        try:
            os.makedirs(os.path.dirname(path))
        except OSError:
            pass
        table_open_object = open(path, 'x')
    pytoml.dump(table_open_object, data)
